package br.com.caelum.twittelum

import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import br.com.caelum.twittelum.database.TweetelumDatabase
import br.com.caelum.twittelum.database.TweetelumDatabase_Impl
import br.com.caelum.twittelum.modelo.Tweet
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.lista_tweet_activity.*

class ListaTweetsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.lista_tweet_activity)


        fab_add.setOnClickListener {
            val intent  = Intent(this, MainActivity::class.java)
            startActivity(intent)
            //Snackbar.make(it,"Fab clickado",Snackbar.LENGTH_LONG).show()
        }


    }

    override fun onResume() {
        super.onResume()

        val tweetDao = TweetelumDatabase.getInstance(this).tweetDao();
        val tweets: List<Tweet> = tweetDao.lista()
        val adapter  = ArrayAdapter<Tweet>(this,android.R.layout.simple_list_item_1,tweets)
        lista_tweet.adapter = adapter
    }
}