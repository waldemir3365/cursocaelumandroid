package br.com.caelum.twittelum.viewmodel

import androidx.lifecycle.ViewModel
import br.com.caelum.twittelum.database.TweetRepository
import br.com.caelum.twittelum.modelo.Tweet

class TweetViewModel(private val repository:TweetRepository): ViewModel() {
    fun lista() = repository.lista()
    fun salva(tweet:Tweet) = repository.salva(tweet)
}