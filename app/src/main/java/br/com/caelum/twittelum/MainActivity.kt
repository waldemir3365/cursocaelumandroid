package br.com.caelum.twittelum

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import br.com.caelum.twittelum.database.TweetelumDatabase
import br.com.caelum.twittelum.modelo.Tweet
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.publicar->publicar()
            android.R.id.home -> finish()
        }
        return true
    }

    private fun publicar(){

        val mensagemDoTweet:String = twitteConteudo.text.toString()

        val tweet = Tweet(mensagemDoTweet)

        val tweetDao = TweetelumDatabase.getInstance(this).tweetDao()
        tweetDao.salva(tweet)

        Toast.makeText(this,"$tweet foi salvo com sucesso",Toast.LENGTH_LONG).show()
        finish()
    }
}

