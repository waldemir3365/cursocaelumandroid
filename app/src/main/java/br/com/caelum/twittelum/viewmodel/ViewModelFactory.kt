package br.com.caelum.twittelum.viewmodel

import androidx.lifecycle.ViewModelProvider
import br.com.caelum.twittelum.aplication.TwittelumApplication
import br.com.caelum.twittelum.database.TweetelumDatabase

object ViewModelFactory: ViewModelProvider.Factory {

    private val database = TweetelumDatabase.getInstance(TwittelumApplication.getInstance())

}