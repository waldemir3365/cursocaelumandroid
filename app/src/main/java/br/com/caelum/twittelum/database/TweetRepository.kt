package br.com.caelum.twittelum.database

import br.com.caelum.twittelum.DAO.TweetDao
import br.com.caelum.twittelum.modelo.Tweet

class TweetRepository(private val fonteDeDados: TweetDao){

    fun lista() = fonteDeDados.lista()
    fun salva(tweet: Tweet) = fonteDeDados.salva(tweet)
}