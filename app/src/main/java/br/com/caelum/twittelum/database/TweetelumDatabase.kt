package br.com.caelum.twittelum.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import br.com.caelum.twittelum.DAO.TweetDao
import br.com.caelum.twittelum.modelo.Tweet
import java.security.AccessControlContext

@Database(entities = [Tweet::class], version = 1)
abstract class TweetelumDatabase: RoomDatabase() {

    abstract  fun tweetDao(): TweetDao


    companion object{

        private var database:TweetelumDatabase? = null
        private val DATABASE = "TwittelumDB"

        fun getInstance(context: Context):TweetelumDatabase{
            return database?:criaBanco(context).also{ database = it}
        }

        private fun criaBanco(context: Context): TweetelumDatabase {
            return Room.databaseBuilder(context,TweetelumDatabase::class.java, DATABASE)
                .allowMainThreadQueries()
                .build()
        }
    }
}